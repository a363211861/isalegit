//
//  PublicFile.h
//  IOSSun
//
//  Created by Y W on 13-4-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#ifndef IOSSun_PublicFile_h
#define IOSSun_PublicFile_h

#import <MobileCoreServices/MobileCoreServices.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "UIColor+Category.h"
#import "UIFont+Category.h"

//真机去掉log
#ifdef DEBUG
#    define NSLog(...) NSLog(__VA_ARGS__)
#else
#    define NSLog(...) {}
#endif

#ifdef DEBUG
#define LogMethod						NSLog(@"%s-%d", __PRETTY_FUNCTION__, __LINE__);
#define LogMethodArgs(format, ... ) 	NSLog(@"%s: %@", __PRETTY_FUNCTION__, [NSString stringWithFormat:format, ##__VA_ARGS__] );
#else
#define LogMethod						{}
#define LogMethodArgs(format, ... )		{}
#endif

#endif
