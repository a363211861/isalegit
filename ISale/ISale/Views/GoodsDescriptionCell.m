//
//  GoodsDescriptionCell.m
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "GoodsDescriptionCell.h"

@implementation GoodsDescriptionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (float)CellHeightForDescription:(NSString *)description
{
    CGRect rect = [description boundingRectWithSize:CGSizeMake(300, 1000) options:NSStringDrawingTruncatesLastVisibleLine attributes:[NSDictionary dictionaryWithObject:[UIFont customFontFour] forKey:NSFontAttributeName] context:nil];
    
    return rect.size.height;
}

@end
