//
//  GoodsDescriptionCell.h
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsDescriptionCell : UITableViewCell

+ (float)CellHeightForDescription:(NSString *)description;

@end
