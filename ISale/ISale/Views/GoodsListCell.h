//
//  GoodsListCell.h
//  ISale
//
//  Created by Y W on 14-2-18.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoodsListCell : UITableViewCell

+ (CGFloat)CellHeight;

@property (nonatomic, strong) UIImageView *iconImageView;

@end
