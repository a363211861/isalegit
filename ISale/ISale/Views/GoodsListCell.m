//
//  GoodsListCell.m
//  ISale
//
//  Created by Y W on 14-2-18.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import "GoodsListCell.h"

#import "UIColor+FlatUI.h"

@interface GoodsListCell ()

@property (nonatomic, assign) BOOL cornerRadius;
@property (nonatomic, assign) CGRect iconRect;

@end

@implementation GoodsListCell

+ (CGFloat)CellHeight
{
    return 60;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor cloudsColor];
        self.contentView.backgroundColor = [UIColor cloudsColor];
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.cornerRadius = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (self.imageView)
    {
        if (!self.cornerRadius) {
            self.iconRect = self.imageView.frame;
            self.imageView.layer.cornerRadius = 5;
            self.cornerRadius = YES;
        }
        self.imageView.frame = self.iconRect;
    }
}

@end
