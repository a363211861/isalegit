//
//  GoodsDetailHeaderCell.h
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Goods;

@interface GoodsDetailHeaderCell : UITableViewCell

@property (nonatomic, readonly, strong) UITextField *textField;

@property (nonatomic, strong) Goods *goods;

+ (float)CellHeight;

@end
