//
//  GoodsCell.h
//  ISale
//
//  Created by Y W on 13-11-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const GoodsCellId;

@interface GoodsCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *imageView;

@end
