//
//  GoodsCell.m
//  ISale
//
//  Created by Y W on 13-11-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "GoodsCell.h"

#import "Goods.h"

NSString * const GoodsCellId = @"GoodsCellId";

@implementation GoodsCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        {
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bounds];
            imageView.backgroundColor = [UIColor whiteColor];
            [self addSubview:imageView];
            self.imageView = imageView;
        }
        
        self.layer.cornerRadius = 5;
        self.clipsToBounds = YES;
    }
    return self;
}

@end
