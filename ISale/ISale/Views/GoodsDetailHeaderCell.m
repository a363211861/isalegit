//
//  GoodsDetailHeaderCell.m
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "GoodsDetailHeaderCell.h"

#import "UIImage+Reflection.h"

@implementation GoodsDetailHeaderCell

+ (float)CellHeight
{
    return 90;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        {
            UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(80, 10, 200, 40)];
            textField.backgroundColor = [UIColor redColor];
            textField.placeholder = @"点此输入商品名";
            [self.contentView addSubview:textField];
            
            _textField = textField;
        }
        
        {
            self.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        }
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

@end
