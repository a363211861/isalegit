//
//  AddInViewController.m
//  ISale
//
//  Created by Y W on 13-9-22.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AddInViewController.h"

#import "UITextField+Category.h"
#import "UIColor+FlatUI.h"
#import "UIButton+Category.h"

@interface AddInViewController () <UITextFieldDelegate>

@property (weak, nonatomic) UITextField *emailTextField;
@property (weak, nonatomic) UITextField *passwordTextField;

@property (weak, nonatomic) UIButton *addInButton;

@end

@implementation AddInViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"注册";
    
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
    
    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 90, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.returnKeyType = UIReturnKeyNext;
        [textField backgroundForNormal];
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"邮   箱";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        [self.view addSubview:textField];
        self.emailTextField = textField;
    }
    
    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 150, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        textField.returnKeyType = UIReturnKeyDone;
        [textField backgroundForNormal];
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"密   码";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        textField.secureTextEntry = YES;
        [self.view addSubview:textField];
        self.passwordTextField = textField;
    }
    
    
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(10, 230, 300, 50);
        [button backgroundWithSystemColor];
        [button setTitle:@"注  册" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(addInAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        self.addInButton = button;
    }
}


#pragma mark - action

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (void)addInAction:(UIButton *)button
{
    [self.view endEditing:YES];
    
    NSString *string = self.emailTextField.text;
    if (string.length == 0 || ![Util isValidateEmail:string]) {
        [self.emailTextField becomeFirstResponder];
        [self.emailTextField backgroundForError];
        return;
    }
    
    string = self.passwordTextField.text;
    if (string.length == 0 || ![Util isValidatePassword:string]) {
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField backgroundForError];
        return;
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField backgroundForEdit];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField backgroundForNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    return YES;
}
@end
