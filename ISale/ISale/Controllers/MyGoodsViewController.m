//
//  MyGoodsViewController.m
//  ISale
//
//  Created by Y W on 13-11-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "MyGoodsViewController.h"

#import "UIColor+FlatUI.h"

#import "GoodsCell.h"

#import "Goods.h"
#import "NetImage.h"
#import "NetImageDownloader.h"

@interface MyGoodsViewController () <UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSMutableArray *entries;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;

@end

@implementation MyGoodsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    switch (self.goodsType) {
        case GoodsTypeSelling:
            self.title = @"拍卖中";
            break;
        case GoodsTypeSold:
            self.title = @"已卖出";
            break;
        case GoodsTypeUnSold:
            self.title = @"已下架";
            break;
        default:
            break;
    }
    
    self.collectionView.backgroundColor = [UIColor cloudsColor];
    self.collectionView.backgroundView = nil;
    
    [self.collectionView registerClass:[GoodsCell class] forCellWithReuseIdentifier:GoodsCellId];
    
    self.entries = [NSMutableArray array];
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
    
    [self getPics];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // terminate all pending download connections
    NSArray *allDownloads = [self.imageDownloadsInProgress allValues];
    [allDownloads makeObjectsPerformSelector:@selector(cancelDownload)];
    
    [self.imageDownloadsInProgress removeAllObjects];
}

- (void)getPics {
	
	UIActivityIndicatorView  *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	activity.center = self.view.center;
	activity.hidesWhenStopped = TRUE;
	[self.view addSubview:activity];
	[activity startAnimating];
	
	NSURL *url = [NSURL URLWithString:@"http://api.flickr.com/services/feeds/photos_public.gne?tags=party&format=json"];
	NSURLRequest *req = [NSURLRequest requestWithURL:url];
	[NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		responseString = [responseString stringByReplacingOccurrencesOfString:@"jsonFlickrFeed" withString:@""];
		responseString = [responseString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
		responseString = [responseString stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];
		
		NSError *jsonError;
		NSData *trimmedData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
		NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trimmedData options:NSJSONReadingAllowFragments error:&jsonError];
		if (jsonError) {
			NSLog(@"JSON parse error: %@", jsonError);
			return;
		}
		
		NSArray *flikrs = [json objectForKey:@"items"];
		NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:[flikrs count]];
		for (NSDictionary *item in flikrs) {
            Goods *goods = [[Goods alloc] init];
            goods.goodsIcon.imageURLString = [[item objectForKey:@"media"] objectForKey:@"m"];
            goods.goodsIcon.size = CGSizeMake(kGoodsCollectionViewCellIconSize, kGoodsCollectionViewCellIconSize);
			[tmp addObject:goods];
		}
		
		[self.entries setArray:tmp];
		NSLog(@"found %d pictures, will download as needed", [tmp count]);
		
		[self.collectionView reloadData];
        
		[activity stopAnimating];
		[activity removeFromSuperview];
	}];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.entries count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:GoodsCellId forIndexPath:indexPath];
    
    Goods *goods = [self.entries objectAtIndex:indexPath.row];
    // Only load cached images; defer new downloads until scrolling ends
    if (!goods.goodsIcon.image)
    {
        if (self.collectionView.dragging == NO && self.collectionView.decelerating == NO)
        {
            [self startIconDownload:goods forIndexPath:indexPath];
        }
        // if a download is deferred or in progress, return a placeholder image
        //cell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        cell.imageView.image = nil;
    }
    else
    {
        cell.imageView.image = goods.goodsIcon.image;
    }
    
	return cell;
}


#pragma mark - UICollectionViewDelegate

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}




#pragma mark - Table cell image support

// -------------------------------------------------------------------------------
//	startIconDownload:forIndexPath:
// -------------------------------------------------------------------------------
- (void)startIconDownload:(Goods *)goods forIndexPath:(NSIndexPath *)indexPath
{
    NetImageDownloader *netImageDownloader = [self.imageDownloadsInProgress objectForKey:indexPath];
    if (netImageDownloader == nil)
    {
        netImageDownloader = [[NetImageDownloader alloc] init];
        netImageDownloader.netImage = goods.goodsIcon;
        [netImageDownloader setCompletionHandler:^{
            
            GoodsCell *cell = (GoodsCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
            
            // Display the newly loaded image
            cell.imageView.image = goods.goodsIcon.image;
            
            // Remove the NetImageDownloader from the in progress list.
            // This will result in it being deallocated.
            [self.imageDownloadsInProgress removeObjectForKey:indexPath];
            
        }];
        [self.imageDownloadsInProgress setObject:netImageDownloader forKey:indexPath];
        [netImageDownloader startDownload];
    }
}

// -------------------------------------------------------------------------------
//	loadImagesForOnscreenRows
//  This method is used in case the user scrolled into a set of cells that don't
//  have their app icons yet.
// -------------------------------------------------------------------------------
- (void)loadImagesForOnscreenRows
{
    if ([self.entries count] > 0)
    {
        NSArray *visiblePaths = [self.collectionView indexPathsForVisibleItems];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            Goods *goods = [self.entries objectAtIndex:indexPath.row];
            
            if (!goods.goodsIcon.image)
                // Avoid the app icon download if the app already has an icon
            {
                [self startIconDownload:goods forIndexPath:indexPath];
            }
        }
    }
}

#pragma mark - UIScrollViewDelegate

// -------------------------------------------------------------------------------
//	scrollViewDidEndDragging:willDecelerate:
//  Load images for all onscreen rows when scrolling is finished.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

// -------------------------------------------------------------------------------
//	scrollViewDidEndDecelerating:
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}

@end
