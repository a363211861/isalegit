//
//  GoodsViewController.m
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "GoodsViewController.h"

#import "GoodsDetailHeaderCell.h"

@interface GoodsViewController ()

@end

@implementation GoodsViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellForHeaderIdentifier = @"CellForHeaderIdentifier";
    static NSString *CellForDescriptionIdentifier = @"CellForDescriptionIdentifier";
    
    if (indexPath.section == 0) {
        GoodsDetailHeaderCell *cellForHeader = [tableView dequeueReusableCellWithIdentifier:CellForHeaderIdentifier];
        if (cellForHeader == nil) {
            cellForHeader = [[GoodsDetailHeaderCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellForHeaderIdentifier];
        }
        return cellForHeader;
    }
    
    
    
    return nil;
}

@end
