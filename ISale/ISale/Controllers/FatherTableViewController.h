//
//  FatherTableViewController.h
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherViewController.h"

@interface FatherTableViewController : FatherViewController

@property (nonatomic, strong) UITableView *tableView;

- (id)initWithStyle:(UITableViewStyle)style;
- (id)initPullToRefresh;

@end
