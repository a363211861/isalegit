//
//  SearchViewController.m
//  ISale
//
//  Created by Y W on 13-10-22.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "SearchViewController.h"
#import "UIImage+Category.h"
#import "UIFont+Category.h"

#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"
#import "UIColor+FlatUI.h"
#import "UIImage+FlatUI.h"


@interface SearchViewController () <UITextFieldDelegate>
{
    UITextField *_searchTextField;
}

@end

@implementation SearchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        view.backgroundColor = [UIColor clearColor];
        
        UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(5, 7, view.bounds.size.width - 10, 30)];
        textField.delegate = self;
        textField.backgroundColor = [UIColor clearColor];
        textField.borderStyle = UITextBorderStyleNone;
        textField.textColor = [UIColor silverColor];
        textField.font = [UIFont boldCustomFontThree];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        textField.keyboardType = UIKeyboardTypeDefault;
        textField.returnKeyType = UIReturnKeySearch;
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 30, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor silverColor];
            label.font = [UIFont fontWithName:kFontAwesomeFamilyName size:15];
            label.text = [NSString fontAwesomeIconStringForEnum:FAIconSearch];
            
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        
        UIImage *image = [UIImage imageWithColor:[UIColor whiteColor] cornerRadius:5];
        image = [image OriginImage:image scaleToSize:CGRectInset(textField.bounds, 10, 5).size];
        textField.background = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
        textField.placeholder = @"搜索";
        [view addSubview:textField];
        
        self.tableView.tableHeaderView = view;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}
@end
