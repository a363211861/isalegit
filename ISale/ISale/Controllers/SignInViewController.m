//
//  SignInViewController.m
//  ISale
//
//  Created by Y W on 13-9-21.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "SignInViewController.h"

#import "UITextField+Category.h"
#import "UIButton+Category.h"
#import "UIBarButtonItem+Category.h"
#import "UIColor+FlatUI.h"

#import "User.h"

#import "AddInViewController.h"
#import "ForgetPasswordViewController.h"

@interface SignInViewController () <UITextFieldDelegate>

@property (weak, nonatomic) UITextField *emailTextField;
@property (weak, nonatomic) UITextField *passwordTextField;

@property (weak, nonatomic) UIButton *signInButton;
@property (weak, nonatomic) UIButton *forgetPasswordButton;

@property (nonatomic, strong) User *user;

@end

@implementation SignInViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"登录";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"注册"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(showAddInViewAction:)];
    [self.navigationItem.rightBarButtonItem customUIConfig];
    
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
    
    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 90, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.returnKeyType = UIReturnKeyNext;
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"邮   箱";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        [self.view addSubview:textField];
        self.emailTextField = textField;
    }
    
    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 150, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        textField.returnKeyType = UIReturnKeyDone;
        [textField backgroundForNormal];
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"密   码";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        textField.secureTextEntry = YES;
        [self.view addSubview:textField];
        self.passwordTextField = textField;
    }
    
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(10, 230, 300, 50);
        button.backgroundColor = [UIColor clearColor];
        [button backgroundWithSystemColor];
        [button setTitle:@"登  录" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(signInAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        self.signInButton = button;
    }
    
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(227, 295, 83, 40);
        [button setTitle:@"忘记密码 >>" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(forgetPasswordAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:button];
        self.forgetPasswordButton = button;
    }
}

#pragma mark - action

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (void)showAddInViewAction:(UIButton *)button
{
    AddInViewController *addInViewController = [[AddInViewController alloc] init];
    [self.navigationController pushViewController:addInViewController animated:YES];
}

- (void)forgetPasswordAction:(UIButton *)button
{
    ForgetPasswordViewController *forgetPasswordViewController = [[ForgetPasswordViewController alloc] init];
    [self.navigationController pushViewController:forgetPasswordViewController animated:YES];
}

- (void)signInAction:(UIButton *)button
{
    [self.view endEditing:YES];
    
    NSString *string = self.emailTextField.text;
    if (string.length == 0 || ![Util isValidateEmail:string]) {
        [self.emailTextField becomeFirstResponder];
        [self.emailTextField backgroundForError];
        return;
    }
    
    string = self.passwordTextField.text;
    if (string.length == 0 || ![Util isValidatePassword:string]) {
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField backgroundForError];
        return;
    }
    
    User *user = [User sharedUser];
    user.email = self.emailTextField.text;
    user.password = self.passwordTextField.text;
    self.user = user;
    
    [self signIn];
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField backgroundForEdit];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField backgroundForNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.emailTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    return YES;
}


#pragma mark - func

- (void)signIn
{
    __weak __typeof(self)weakSelf = self;
    [self.user signInWithFinishBlock:^(FatherModel *model, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (error) {
            [strongSelf showStatus:[error localizedDescription]];
        } else if ([model.code intValue] == 0) {
            [strongSelf signInSuccess];
        } else {
            [strongSelf signInFail];
        }
    }];
}

- (void)signInFail
{
    if ([self.user.alert boolValue]) {
        [self showErrorWithStatus:self.user.message];
    }
}

- (void)signInSuccess
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

@end
