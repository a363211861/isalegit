//
//  ForgetPasswordViewController.m
//  ISale
//
//  Created by Y W on 13-9-22.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "ForgetPasswordViewController.h"

#import "UITextField+Category.h"
#import "UIButton+Category.h"
#import "UIColor+FlatUI.h"

@interface ForgetPasswordViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) UIView *stepOneView;

@property (weak, nonatomic) UILabel *noticeLabel;
@property (weak, nonatomic) UILabel *emailLabel;
@property (weak, nonatomic) UITextField *codeTextField;
@property (weak, nonatomic) UIButton *nextButton;


@property (weak, nonatomic) UIView *stepTwoView;
@property (weak, nonatomic) UITextField *passwordTextField;
@property (weak, nonatomic) UITextField *rePasswordTextField;
@property (weak, nonatomic) UIButton *finishButton;

@end

@implementation ForgetPasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"忘记密码";
    
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureRecognizerAction:)];
        [self.view addGestureRecognizer:tapGestureRecognizer];
    }
    
    //step one view
    {
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.backgroundColor = [UIColor clearColor];
        [self.view addSubview:view];
        
        self.stepOneView = view;
    }
    
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 86, 300, 20)];
        label.font = [UIFont boldCustomFontThree];
        label.textColor = [UIColor silverColor];
        label.shadowColor = [UIColor whiteColor];
        label.shadowOffset = CGSizeMake(0, 1);
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = @"邮箱验证码已经发送到您的邮箱";
        
        [self.stepOneView addSubview:label];
        self.noticeLabel = label;
    }
    
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 116, 300, 20)];
        label.font = [UIFont boldCustomFontThree];
        label.textColor = [UIColor midnightBlueColor];
        label.shadowColor = [UIColor whiteColor];
        label.shadowOffset = CGSizeMake(0, 1);
        label.numberOfLines = 0;
        label.textAlignment = NSTextAlignmentCenter;
        label.text = self.email;
        
        [self.stepOneView addSubview:label];
        self.emailLabel = label;
    }

    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 154, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.returnKeyType = UIReturnKeyDone;
        textField.placeholder = @"您收到的验证码";
        [textField backgroundForNormal];
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"验证码";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        [self.stepOneView addSubview:textField];
        self.codeTextField = textField;
    }
    
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(10, 224, 300, 50);
        button.backgroundColor = [UIColor clearColor];
        [button backgroundWithSystemColor];
        [button setTitle:@"下一步" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.stepOneView addSubview:button];
        self.nextButton = button;
    }
    
    //step two view
    {
        UIView *view = [[UIView alloc] initWithFrame:self.view.bounds];
        view.backgroundColor = [UIColor clearColor];
        [self.view addSubview:view];
        
        self.stepTwoView = view;
    }
    
    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 90, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        textField.returnKeyType = UIReturnKeyNext;
        [textField backgroundForNormal];
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"密码";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        textField.placeholder = @"输入您的密码";
        textField.secureTextEntry = YES;
        [self.stepTwoView addSubview:textField];
        self.passwordTextField = textField;
    }
    
    {
        UITextField *textField = [UITextField textField];
        textField.frame = CGRectMake(10, 150, 300, 50);
        textField.delegate = self;
        textField.keyboardType = UIKeyboardTypeASCIICapable;
        textField.returnKeyType = UIReturnKeyDone;
        [textField backgroundForNormal];
        {
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, textField.frame.size.height)];
            label.backgroundColor = [UIColor clearColor];
            label.textAlignment = NSTextAlignmentCenter;
            label.textColor = [UIColor midnightBlueColor];
            label.font = [UIFont boldCustomFontTwo];
            label.text = @"验证";
            textField.leftView = label;
            textField.leftViewMode = UITextFieldViewModeAlways;
        }
        textField.placeholder = @"输入您的密码";
        textField.secureTextEntry = YES;
        [self.stepTwoView addSubview:textField];
        self.rePasswordTextField = textField;
    }
    
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
        button.frame = CGRectMake(10, 230, 300, 50);
        [button backgroundWithSystemColor];
        [button setTitle:@"完成" forState:UIControlStateNormal];
        [button addTarget:self action:@selector(finishAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.stepTwoView addSubview:button];
        self.finishButton = button;
    }
    
    self.stepTwoView.hidden = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - action

- (void)tapGestureRecognizerAction:(UITapGestureRecognizer *)tapGestureRecognizer
{
    [self.view endEditing:YES];
}

- (void)nextAction:(UIButton *)button
{
    NSString *string = self.codeTextField.text;
    if (string.length == 0) {
        [self.codeTextField becomeFirstResponder];
        [self.codeTextField backgroundForError];
        return;
    } else {
        [self.codeTextField backgroundForNormal];
    }
    
    self.stepTwoView.hidden = NO;
    self.stepTwoView.alpha = 0;
    [UIView animateWithDuration:0.25 animations:^{
        self.stepTwoView.alpha = 1;
        self.stepOneView.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            self.stepOneView.hidden = YES;
        }
    }];
}

- (void)finishAction:(UIButton *)button
{
    [self.view endEditing:YES];
    
    NSString *password = self.passwordTextField.text;
    if (password.length == 0) {
        [self.passwordTextField becomeFirstResponder];
        [self.passwordTextField backgroundForError];
        return;
    }
    
    NSString *rePassword = self.rePasswordTextField.text;
    if (rePassword.length == 0) {
        [self.rePasswordTextField becomeFirstResponder];
        [self.rePasswordTextField backgroundForError];
        return;
    }
    
    if (![password isEqualToString:rePassword]) {
        [self.rePasswordTextField becomeFirstResponder];
        [self.rePasswordTextField backgroundForError];
        [self alert:@"两次输入密码不相同"];
        return;
    }
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [textField backgroundForEdit];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField backgroundForNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.passwordTextField) {
        [self.rePasswordTextField becomeFirstResponder];
    }
    
    return YES;
}


@end
