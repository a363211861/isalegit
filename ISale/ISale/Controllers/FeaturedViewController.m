//
//  FeaturedViewController.m
//  ISale
//
//  Created by Y W on 13-10-27.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FeaturedViewController.h"

#import "Networking.h"

#import "Goods.h"
#import "NetImage.h"
#import "NetImageDownloader.h"

#import "GoodsListCell.h"
#import "ETActivityIndicatorView.h"

@interface FeaturedViewController ()

@property (nonatomic, strong) Networking *networking;

@property (nonatomic, strong) NSMutableArray *entries;
@property (nonatomic, strong) NSMutableDictionary *imageDownloadsInProgress;

@end

@implementation FeaturedViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.entries = [NSMutableArray array];
    self.imageDownloadsInProgress = [NSMutableDictionary dictionary];
    
    self.networking = [[Networking alloc] initGetParameters:[NSDictionary dictionaryWithObject:@"tmall.selected.items.search" forKey:@"method"] finishBlock:^(id response, NSError *error) {
        ;
    }];
}

#pragma mark - funcs

- (void)refresh
{
    [self.entries removeAllObjects];
    [self.imageDownloadsInProgress removeAllObjects];
    [self.tableView reloadData];
    
    [self getPics];
}

#pragma mark - network

- (void)getPics {
	
	if (0) {
        UIActivityIndicatorView  *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activity.center = self.view.center;
        activity.hidesWhenStopped = TRUE;
        [self.view addSubview:activity];
        [activity startAnimating];
    }
    
    ETActivityIndicatorView *etActivity = [[ETActivityIndicatorView alloc] initWithFrame:CGRectMake(ceilf((CGRectGetWidth(self.view.bounds) - ETActivityDefaultSize)/2), ceilf((CGRectGetHeight(self.view.bounds) - ETActivityDefaultSize)/2), ETActivityDefaultSize, ETActivityDefaultSize)];
    [etActivity startAnimating];
    [self.view addSubview:etActivity];
	
	NSURL *url = [NSURL URLWithString:@"http://api.flickr.com/services/feeds/photos_public.gne?tags=party&format=json"];
	NSURLRequest *req = [NSURLRequest requestWithURL:url];
	[NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
		NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
		responseString = [responseString stringByReplacingOccurrencesOfString:@"jsonFlickrFeed" withString:@""];
		responseString = [responseString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"()"]];
		responseString = [responseString stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];
		
		NSError *jsonError;
		NSData *trimmedData = [responseString dataUsingEncoding:NSUTF8StringEncoding];
		NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trimmedData options:NSJSONReadingAllowFragments error:&jsonError];
		if (jsonError) {
			NSLog(@"JSON parse error: %@", jsonError);
			return;
		}
		
		NSArray *flikrs = [json objectForKey:@"items"];
		NSMutableArray *tmp = [NSMutableArray arrayWithCapacity:[flikrs count]];
		for (NSDictionary *item in flikrs) {
            Goods *goods = [[Goods alloc] init];
            goods.goodsIcon.imageURLString = [[item objectForKey:@"media"] objectForKey:@"m"];
            goods.goodsIcon.size = CGSizeMake(kGoodsListCellIconSize, kGoodsListCellIconSize);
			[tmp addObject:goods];
		}
		
		[self.entries setArray:tmp];
		NSLog(@"found %d pictures, will download as needed", [tmp count]);
		
		[self.tableView reloadData];
        [self refreshFinish];
        
        [etActivity stopAnimating];
        [etActivity removeFromSuperview];
	}];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.entries.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    GoodsListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[GoodsListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Goods *goods = [self.entries objectAtIndex:indexPath.row];
    // Only load cached images; defer new downloads until scrolling ends
    if (!goods.goodsIcon.image)
    {
        if (self.tableView.dragging == NO && self.tableView.decelerating == NO)
        {
            [self startIconDownload:goods forIndexPath:indexPath];
        }
        // if a download is deferred or in progress, return a placeholder image
        //cell.imageView.image = [UIImage imageNamed:@"Placeholder.png"];
        cell.imageView.image = [UIImage imageNamed:@"Placeholder"];
    }
    else
    {
        cell.imageView.image = goods.goodsIcon.image;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [GoodsListCell CellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Table cell image support

// -------------------------------------------------------------------------------
//	loadImagesForOnscreenRows
//  This method is used in case the user scrolled into a set of cells that don't
//  have their app icons yet.
// -------------------------------------------------------------------------------
- (void)loadImagesForOnscreenRows
{
    if ([self.entries count] > 0)
    {
        NSArray *visiblePaths = [self.tableView indexPathsForVisibleRows];
        for (NSIndexPath *indexPath in visiblePaths)
        {
            Goods *goods = [self.entries objectAtIndex:indexPath.row];
            
            if (!goods.goodsIcon.image)
                // Avoid the app icon download if the app already has an icon
            {
                [self startIconDownload:goods forIndexPath:indexPath];
            }
        }
    }
}

// -------------------------------------------------------------------------------
//	startIconDownload:forIndexPath:
// -------------------------------------------------------------------------------
- (void)startIconDownload:(Goods *)goods forIndexPath:(NSIndexPath *)indexPath
{
    NetImageDownloader *netImageDownloader = [self.imageDownloadsInProgress objectForKey:indexPath];
    if (netImageDownloader == nil)
    {
        netImageDownloader = [[NetImageDownloader alloc] init];
        netImageDownloader.netImage = goods.goodsIcon;
        [netImageDownloader setCompletionHandler:^{
            
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            
            // Display the newly loaded image
            cell.imageView.image = goods.goodsIcon.image;
            
            // Remove the NetImageDownloader from the in progress list.
            // This will result in it being deallocated.
            [self.imageDownloadsInProgress removeObjectForKey:indexPath];
            
        }];
        [self.imageDownloadsInProgress setObject:netImageDownloader forKey:indexPath];
        [netImageDownloader startDownload];
    }
}


#pragma mark - UIScrollViewDelegate

// -------------------------------------------------------------------------------
//	scrollViewDidEndDragging:willDecelerate:
//  Load images for all onscreen rows when scrolling is finished.
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
	{
        [self loadImagesForOnscreenRows];
    }
}

// -------------------------------------------------------------------------------
//	scrollViewDidEndDecelerating:
// -------------------------------------------------------------------------------
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self loadImagesForOnscreenRows];
}
@end
