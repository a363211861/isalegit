//
//  ForgetPasswordViewController.h
//  ISale
//
//  Created by Y W on 13-9-22.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherViewController.h"

@interface ForgetPasswordViewController : FatherViewController

@property (nonatomic, strong) NSString *email;

@end
