//
//  FatherViewController.m
//  IOSSun
//
//  Created by Y W on 13-3-24.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherViewController.h"

#import "SVProgressHUD.h"
#import "UIColor+FlatUI.h"

#import "RefreshDateControl.h"


static const int defaultRefreshSecs = 300;


#pragma mark - FatherViewController private category

@interface FatherViewController (refreshPrivate)

- (void)refreshControlFunction;

@end





#pragma mark - FatherViewController extension

@interface FatherViewController ()

@property (nonatomic, assign) NSTimeInterval needRefreshSecs;
@property (nonatomic, strong) NSString *classDateMD5;
@property (nonatomic, strong) RefreshDateControl *refreshControl;

@end




#pragma mark - FatherViewController

@implementation FatherViewController

- (id)init
{
    self = [super init];
    if (self) {
        [self customInitFuntions];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self customInitFuntions];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self customInitFuntions];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor cloudsColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.leftBarButtonItem.enabled = NO;
	self.navigationItem.rightBarButtonItem.enabled = NO;
    self.view.userInteractionEnabled = NO;
    
    [self refreshControlFunction];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.navigationItem.leftBarButtonItem.enabled = YES;
	self.navigationItem.rightBarButtonItem.enabled = YES;
    self.view.userInteractionEnabled = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
    
	self.navigationItem.leftBarButtonItem.enabled = NO;
	self.navigationItem.rightBarButtonItem.enabled = NO;
    self.view.userInteractionEnabled = NO;
}

- (void)dealloc
{
    [self.refreshControl deleteRefreshDateWithClassDateMD5:self.classDateMD5];
}

- (void)customInitFuntions
{
    self.needRefreshSecs = defaultRefreshSecs;
    
    NSString *classDateString = [NSString stringWithFormat:@"%@%@", [self class], [Util stringFromDate:[NSDate date] withDateFormat:@"yyyyMMddHHmmss"]];
    self.classDateMD5 = [Util md5StringForString:classDateString];
    self.refreshControl = [RefreshDateControl sharedRefreshDateControl];
}

@end




#pragma mark - alert category


@implementation FatherViewController (alert)

-(void)alert:(NSString *)alertString
{
    if (alertString == nil || ![alertString isKindOfClass:[NSString class]]) {
        return;
    }
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:alertString delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil];
    [alertView show];
}

- (void)showSuccessWithStatus:(NSString*)string
{
    [SVProgressHUD showSuccessWithStatus:string];
}

- (void)showErrorWithStatus:(NSString *)string
{
    [SVProgressHUD showErrorWithStatus:string];
}

- (void)showStatus:(NSString *)string
{
    [SVProgressHUD showImage:nil status:string];
}

@end




#pragma mark - network category


@implementation FatherViewController (refreshPrivate)

- (void)refreshControlFunction
{
    NSDate *lastRefreshDate = [self.refreshControl getRefreshDateWithClassDateMD5:self.classDateMD5];
    if (lastRefreshDate == nil) {
        [self refresh];
    } else {
        if ([[NSDate date] timeIntervalSinceDate:lastRefreshDate] > self.needRefreshSecs) {
            [self refresh];
        }
    }
}

@end




@implementation FatherViewController (network)


-(void)setNeedRefreshDate:(NSTimeInterval)secs
{
    if (secs < 0) {
        secs = defaultRefreshSecs;
    }
    self.needRefreshSecs = secs;
}


- (void)refreshFinish
{
    [self.refreshControl setRefreshDate:[NSDate date] classDateMD5:self.classDateMD5];
}


-(void)refresh
{
    
}

@end