//
//  FatherTableViewController.m
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherTableViewController.h"

@interface FatherTableViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, assign) BOOL needPullToRefresh;
@property (nonatomic, assign) UITableViewStyle tableViewStytle;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@end

@implementation FatherTableViewController

- (id)init
{
    self = [super init];
    if (self) {
        self.tableViewStytle = UITableViewStylePlain;
        self.needPullToRefresh = NO;
    }
    return self;
}

- (id)initPullToRefresh
{
    self = [super init];
    if (self) {
        self.tableViewStytle = UITableViewStylePlain;
        self.needPullToRefresh = YES;
    }
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super init];
    if (self) {
        self.tableViewStytle = style;
        self.needPullToRefresh = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    {
        UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:self.tableViewStytle];
        tableView.backgroundColor = [UIColor clearColor];
        tableView.backgroundView = nil;
        tableView.dataSource = self;
        tableView.delegate = self;
        [Util setExtraCellLineHidden:tableView];
        [self.view addSubview:tableView];
        self.tableView = tableView;
    }
    
    if (self.needPullToRefresh) {
        UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
        refreshControl.attributedTitle = [[NSAttributedString alloc]initWithString:@"下拉刷新"];
        [refreshControl addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
        [self.tableView addSubview:refreshControl];
        self.refreshControl = refreshControl;
    }
}

- (void)handleRefresh:(UIRefreshControl *)refreshControl
{
    [self refresh];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    return cell;
}

@end