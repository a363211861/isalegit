//
//  FatherViewController.h
//  IOSSun
//
//  Created by Y W on 13-3-24.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "Util.h"

@interface FatherViewController : UIViewController

@end


@interface FatherViewController (alert)

- (void)alert:(NSString *)alertString;
- (void)showSuccessWithStatus:(NSString*)string;
- (void)showErrorWithStatus:(NSString *)string;
- (void)showStatus:(NSString *)string;

@end



@interface FatherViewController (Refresh)

- (void)setNeedRefreshDate:(NSTimeInterval)secs;
- (void)refreshFinish;
- (void)refresh;

@end