//
//  MyGoodsViewController.h
//  ISale
//
//  Created by Y W on 13-11-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum _GoodsType {
    GoodsTypeSelling = 0,
    GoodsTypeSold,
    GoodsTypeUnSold,
}GoodsType;

@interface MyGoodsViewController : UICollectionViewController

@property (nonatomic, assign) GoodsType goodsType;

@end
