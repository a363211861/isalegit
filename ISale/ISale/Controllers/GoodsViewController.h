//
//  GoodsViewController.h
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherTableViewController.h"

typedef enum _GoodsViewControllerType
{
    GoodsViewControllerTypeAdd = 0,
    GoodsViewControllerTypeDetail
}GoodsViewControllerType;

@interface GoodsViewController : FatherTableViewController

@property (nonatomic, assign) GoodsViewControllerType *goodsViewControllerType;

@end
