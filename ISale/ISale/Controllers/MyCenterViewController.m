//
//  MyCenterViewController.m
//  ISale
//
//  Created by Y W on 13-10-22.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "MyCenterViewController.h"

#import "UIFont+FontAwesome.h"
#import "NSString+FontAwesome.h"
#import "UIColor+FlatUI.h"

#import "MyGoodsViewController.h"
#import "AddGoodsViewController.h"

@interface MyCenterViewController ()

@end

@implementation MyCenterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    {
        UIBarButtonItem *buttonItem = [[UIBarButtonItem alloc] initWithTitle:@"我要卖" style:UIBarButtonItemStylePlain target:self action:@selector(addAction:)];
        self.navigationItem.rightBarButtonItem = buttonItem;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Actions

- (void)addAction:(id)sender
{
    AddGoodsViewController *addGoodsViewController = [[AddGoodsViewController alloc] init];
    [self.navigationController pushViewController:addGoodsViewController animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *title = @"";
    if (section == 0) {
        title = @"卖";
    } else {
        title = @"买";
    }
    return title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger numberOfRows = 0;
    if (section == 0) {
        numberOfRows = 3;
    } else if (section == 1) {
        numberOfRows = 3;
    }
    return numberOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont customFontOne];
        cell.textLabel.textColor = [UIColor midnightBlueColor];
        cell.textLabel.highlightedTextColor = [UIColor whiteColor];
    }
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"拍卖中";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"已卖出";
        } else if (indexPath.row == 2) {
            cell.textLabel.text = @"未卖出";
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"正在拍";
        } else if (indexPath.row == 1) {
            cell.textLabel.text = @"已拍到";
        } else if (indexPath.row == 2) {
            cell.textLabel.text = @"未拍到";
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
            UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
            [aFlowLayout setItemSize:CGSizeMake(145, 145)];
            [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
            MyGoodsViewController *myGoodsViewController = [[MyGoodsViewController alloc] initWithCollectionViewLayout:aFlowLayout];
            myGoodsViewController.goodsType = indexPath.row;
            [self.navigationController pushViewController:myGoodsViewController animated:YES];
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2) {
            UICollectionViewFlowLayout *aFlowLayout = [[UICollectionViewFlowLayout alloc] init];
            [aFlowLayout setItemSize:CGSizeMake(145, 145)];
            [aFlowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
            MyGoodsViewController *myGoodsViewController = [[MyGoodsViewController alloc] initWithCollectionViewLayout:aFlowLayout];
            myGoodsViewController.goodsType = indexPath.row;
            [self.navigationController pushViewController:myGoodsViewController animated:YES];
        }
    }
}

@end
