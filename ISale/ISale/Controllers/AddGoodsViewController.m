//
//  AddGoodsViewController.m
//  ISale
//
//  Created by Y W on 13-11-24.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AddGoodsViewController.h"

@interface AddGoodsViewController ()

@end

@implementation AddGoodsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
