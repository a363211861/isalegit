//
//  UIBarButtonItem+Category.m
//  ISale
//
//  Created by Y W on 14-2-17.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import "UIBarButtonItem+Category.h"

@implementation UIBarButtonItem (CustomUIConfig)

- (void)customUIConfig
{
    NSMutableDictionary *titleTextAttributes = [NSMutableDictionary dictionaryWithDictionary:[self titleTextAttributesForState:UIControlStateNormal]];
    NSLog(@"%@", [[titleTextAttributes objectForKey:NSFontAttributeName] description]);
    [titleTextAttributes setValue:[UIFont customFontTwo] forKey:NSFontAttributeName];
    [self setTitleTextAttributes:titleTextAttributes forState:UIControlStateNormal];
}

@end
