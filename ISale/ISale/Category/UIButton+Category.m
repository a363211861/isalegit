//
//  UIButton+Category.m
//  ISale
//
//  Created by Y W on 14-2-22.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import "UIButton+Category.h"

#import "UIImage+FlatUI.h"

@implementation UIButton (CustomBackground)

- (void)backgroundForState:(UIControlState)state1 withState:(UIControlState)state2
{
    UIImage *image = [UIImage buttonImageWithColor:[UIColor whiteColor] cornerRadius:2 shadowColor:[self titleColorForState:state2]  shadowInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
    [self setBackgroundImage:[image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2] forState:state1];
}

- (void)backgroundWithSystemHighlightColor
{
    [self backgroundForState:UIControlStateNormal withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateHighlighted withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateSelected withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateDisabled withState:UIControlStateSelected];
}

- (void)backgroundWithSystemColor
{
    [self backgroundForState:UIControlStateNormal withState:UIControlStateNormal];
    [self backgroundForState:UIControlStateHighlighted withState:UIControlStateHighlighted];
    [self backgroundForState:UIControlStateSelected withState:UIControlStateSelected];
    [self backgroundForState:UIControlStateDisabled withState:UIControlStateDisabled];
}

@end
