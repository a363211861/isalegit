//
//  NSObject+Category.m
//  ISale
//
//  Created by Y W on 14-2-10.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import "NSObject+Category.h"

#import <objc/runtime.h>

@implementation NSObject (KVC)

- (id)valueForUndefinedKey:(NSString *)key
{
    return objc_getAssociatedObject(self, (__bridge const void *)(key));
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key
{
    objc_setAssociatedObject(self, (__bridge const void *)(key), value, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
