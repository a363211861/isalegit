//
//  UINavigationController+Category.h
//  ISale
//
//  Created by Y W on 13-9-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UINavigationController (CustomUIConfig)

- (void)customUIConfig;

@end
