//
//  UIFont+Category.m
//  ISale
//
//  Created by Y W on 13-9-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "UIFont+Category.h"

#import "UIFont+FlatUI.h"

@interface NSString (stringCompare)

- (NSComparisonResult)stringCompare:(NSString *)string;

@end

@implementation NSString (stringCompare)

- (NSComparisonResult)stringCompare:(NSString *)string
{
    return [self caseInsensitiveCompare:string];
}

@end

static NSString *customBoldFontName = nil;//@"STHeitiSC-Medium";
static NSString *customFontName = nil;//@"STHeitiSC-Light";

@implementation UIFont (quickly)

+ (void)logAllFontName
{
    NSArray *familyNames = [UIFont familyNames];    // 获取所有的familyName
    familyNames = [familyNames sortedArrayUsingSelector:@selector(stringCompare:)]; // 排序
    NSMutableArray *fontNames = [[NSMutableArray alloc] init];
    for (int i = 0; i < familyNames.count; ++i)
    {
        NSArray* fontArray = [UIFont fontNamesForFamilyName:[self.familyNames objectAtIndex:i]]; // 获取相应的familyName下的fontNames
        [fontNames addObjectsFromArray:fontArray];   // 将fontName存入数组
    }
    
    for (NSString *string in fontNames) {
        NSLog(@"%@", string);
    }
}

+ (UIFont *)customBoldFontOfSize:(CGFloat)fontSize
{
    if (customBoldFontName) {
        return [UIFont fontWithName:customBoldFontName size:fontSize];
    } else {
        return [UIFont boldSystemFontOfSize:fontSize];
    }
}

+ (UIFont *)customFontOfSize:(CGFloat)fontSize
{
    if (customFontName) {
        return [UIFont fontWithName:customFontName size:fontSize];
    } else {
        return [UIFont systemFontOfSize:fontSize];
    }
}



+ (UIFont *)boldCustomFontOne
{
    return [UIFont customBoldFontOfSize:18];
}

+ (UIFont *)boldCustomFontTwo
{
    return [UIFont customBoldFontOfSize:16];
}

+ (UIFont *)boldCustomFontThree
{
    return [UIFont customBoldFontOfSize:15];
}

+ (UIFont *)boldCustomFontFour
{
    return [UIFont customBoldFontOfSize:14];
}

+ (UIFont *)boldCustomFontFive
{
    return [UIFont customBoldFontOfSize:12];
}

+ (UIFont *)customFontOne
{
    return [UIFont customFontOfSize:18];
}

+ (UIFont *)customFontTwo
{
    return [UIFont customFontOfSize:16];
}

+ (UIFont *)customFontThree
{
    return [UIFont customFontOfSize:15];
}

+ (UIFont *)customFontFour
{
    return [UIFont customFontOfSize:14];
}

+ (UIFont *)customFontFive
{
    return [UIFont customFontOfSize:12];
}

@end
