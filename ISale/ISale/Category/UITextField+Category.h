//
//  UITextField+Category.h
//  ISale
//
//  Created by Y W on 13-10-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UITextField (CreateSelf)

+ (UITextField *)textField;

@end

@interface UITextField (CustomBackground)

- (void)backgroundForNormal;
- (void)backgroundForEdit;
- (void)backgroundForError;

@end


@interface UITextField (Shake)

- (void)shake;

@end