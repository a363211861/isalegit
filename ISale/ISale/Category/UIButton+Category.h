//
//  UIButton+Category.h
//  ISale
//
//  Created by Y W on 14-2-22.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIButton (CustomBackground)

- (void)backgroundWithSystemHighlightColor;
- (void)backgroundWithSystemColor;

@end
