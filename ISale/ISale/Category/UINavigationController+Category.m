//
//  UINavigationController+Category.m
//  ISale
//
//  Created by Y W on 13-9-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "UINavigationController+Category.h"

#import "UIColor+FlatUI.h"

@implementation UINavigationController (CustomUIConfig)

- (void)customUIConfig
{
    NSMutableDictionary *titleTextAttributes = [NSMutableDictionary dictionaryWithDictionary:self.navigationBar.titleTextAttributes];
    [titleTextAttributes setObject:[UIFont boldCustomFontOne] forKey:NSFontAttributeName];
    self.navigationBar.titleTextAttributes = titleTextAttributes;
}

@end
