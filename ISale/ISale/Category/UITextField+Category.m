//
//  UITextField+Category.m
//  ISale
//
//  Created by Y W on 13-10-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "UITextField+Category.h"
#import "UIColor+FlatUI.h"
#import "UIImage+FlatUI.h"

@implementation UITextField (CreateSelf)

+ (UITextField *)textField
{
    UITextField *textField = [[UITextField alloc] init];
    textField.backgroundColor = [UIColor clearColor];
    textField.borderStyle = UITextBorderStyleNone;
    textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.enablesReturnKeyAutomatically = YES;
    textField.textColor = [UIColor silverColor];
    textField.font = [UIFont boldCustomFontThree];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [textField backgroundForNormal];
    
    return textField;
}

@end

@implementation UITextField (CustomBackground)

- (void)backgroundForNormal
{
    UIImage *image = [UIImage buttonImageWithColor:[UIColor whiteColor] cornerRadius:2 shadowColor:[UIColor silverColor]  shadowInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
    self.background = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
}

- (void)backgroundForEdit
{
    UIImage *image = [UIImage buttonImageWithColor:[UIColor whiteColor] cornerRadius:2 shadowColor:[UIColor colorWithR:59 G:198 B:81 A:1]  shadowInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
    self.background = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
}

- (void)backgroundForError
{
    UIImage *image = [UIImage buttonImageWithColor:[UIColor whiteColor] cornerRadius:2 shadowColor:[UIColor alizarinColor]  shadowInsets:UIEdgeInsetsMake(1, 1, 1, 1)];
    self.background = [image stretchableImageWithLeftCapWidth:image.size.width/2 topCapHeight:image.size.height/2];
    [self shake];
}

@end


@implementation  UITextField (YHShakeUITextField)

- (void) shake
{
    CAKeyframeAnimation *keyAn = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    [keyAn setDuration:0.5f];
    NSArray *array = [[NSArray alloc] initWithObjects:
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x-5, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x+5, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x-5, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x+5, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x-5, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x+5, self.center.y)],
                      [NSValue valueWithCGPoint:CGPointMake(self.center.x, self.center.y)],
                      nil];
    [keyAn setValues:array];
    
    NSArray *times = [[NSArray alloc] initWithObjects:
                      [NSNumber numberWithFloat:0.1f],
                      [NSNumber numberWithFloat:0.2f],
                      [NSNumber numberWithFloat:0.3f],
                      [NSNumber numberWithFloat:0.4f],
                      [NSNumber numberWithFloat:0.5f],
                      [NSNumber numberWithFloat:0.6f],
                      [NSNumber numberWithFloat:0.7f],
                      [NSNumber numberWithFloat:0.8f],
                      [NSNumber numberWithFloat:0.9f],
                      [NSNumber numberWithFloat:1.0f],
                      nil];
    [keyAn setKeyTimes:times];
    
    [self.layer addAnimation:keyAn forKey:@"TextAnim"];
}

@end