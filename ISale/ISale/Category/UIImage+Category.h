//
//  UIImage+Category.h
//  ISale
//
//  Created by Y W on 13-9-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (Util)

+ (instancetype)imageWithView:(UIView *)view;

@end


@interface UIImage (Scale)

- (UIImage *)OriginImage:(UIImage *)image scaleToSize:(CGSize)size;

@end
