//
//  NSObject+Category.h
//  ISale
//
//  Created by Y W on 14-2-10.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (KVC)

- (id)valueForUndefinedKey:(NSString *)key;

- (void)setValue:(id)value forUndefinedKey:(NSString *)key;

@end
