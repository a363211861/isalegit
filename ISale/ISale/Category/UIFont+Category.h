//
//  UIFont+Category.h
//  ISale
//
//  Created by Y W on 13-9-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIFont (Quickly)

+ (void)logAllFontName;

+ (UIFont *)boldCustomFontOne;
+ (UIFont *)boldCustomFontTwo;
+ (UIFont *)boldCustomFontThree;
+ (UIFont *)boldCustomFontFour;
+ (UIFont *)boldCustomFontFive;

+ (UIFont *)customFontOne;
+ (UIFont *)customFontTwo;
+ (UIFont *)customFontThree;
+ (UIFont *)customFontFour;
+ (UIFont *)customFontFive;

@end