//
//  UIBarButtonItem+Category.h
//  ISale
//
//  Created by Y W on 14-2-17.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIBarButtonItem (CustomUIConfig)

- (void)customUIConfig;

@end
