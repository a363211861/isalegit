//
//  AppDelegate.m
//  ISale
//
//  Created by Y W on 13-8-18.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "AppDelegate.h"

#import "User.h"

#import "UINavigationController+Category.h"
#import "UIImage+FontAwesome.h"
#import "UIColor+FlatUI.h"

#import "SVProgressHUD.h"
#import "AFNetworkActivityIndicatorManager.h"

#import "FeaturedViewController.h"
#import "CategoriesViewController.h"
#import "TopViewController.h"
#import "SearchViewController.h"
#import "MyCenterViewController.h"
#import "SignInViewController.h"

#define customTabBar

@interface AppDelegate ()

@property (nonatomic, strong) User *user;

@end


@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[AFNetworkActivityIndicatorManager sharedManager] setEnabled:YES];
    [self configSVProgressHUD];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self showTabBarController];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    [User sharedUser].resignActiveDate = [NSDate date];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    //return;
    [self checkUserStatus];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - func

- (void)configSVProgressHUD
{
    [[SVProgressHUD appearance] setHudFont:[UIFont boldCustomFontTwo]];
    [[SVProgressHUD appearance] setHudBackgroundColor:[UIColor midnightBlueColor]];
    [[SVProgressHUD appearance] setHudForegroundColor:[UIColor cloudsColor]];
    [[SVProgressHUD appearance] setHudStatusShadowColor:[UIColor midnightBlueColor]];
}

- (void)showTabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    {
        FeaturedViewController *viewController = [[FeaturedViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"推荐";
        
        UIImage *image = [UIImage imageFontAwesomeIconStringForEnum:FAIconThumbsUpAlt color:[UIColor cloudsColor] size:30];
        UIImage *selectImage = [UIImage imageFontAwesomeIconStringForEnum:FAIconThumbsUpAlt color:[UIColor blueColor] size:30];
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:viewController.title image:image selectedImage:selectImage];
        navigationController.tabBarItem = item;
        [viewControllers addObject:navigationController];
    }
    
    {
        CategoriesViewController *viewController = [[CategoriesViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"类别";
        
        UIImage *image = [UIImage imageFontAwesomeIconStringForEnum:FAIconListUl color:[UIColor cloudsColor] size:30];
        UIImage *selectImage = [UIImage imageFontAwesomeIconStringForEnum:FAIconListUl color:[UIColor blueColor] size:30];
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:viewController.title image:image selectedImage:selectImage];
        navigationController.tabBarItem = item;
        [viewControllers addObject:navigationController];
    }
    
    {
        TopViewController *viewController = [[TopViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"热门";
        
        UIImage *image = [UIImage imageFontAwesomeIconStringForEnum:FAIconStarEmpty color:[UIColor cloudsColor] size:30];
        UIImage *selectImage = [UIImage imageFontAwesomeIconStringForEnum:FAIconStarEmpty color:[UIColor blueColor] size:30];
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:viewController.title image:image selectedImage:selectImage];
        navigationController.tabBarItem = item;
        [viewControllers addObject:navigationController];
    }
    
    {
        SearchViewController *viewController = [[SearchViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"搜索";
        UIImage *image = [UIImage imageFontAwesomeIconStringForEnum:FAIconSearch color:[UIColor cloudsColor] size:30];
        UIImage *selectImage = [UIImage imageFontAwesomeIconStringForEnum:FAIconSearch color:[UIColor blueColor] size:30];
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:viewController.title image:image selectedImage:selectImage];
        navigationController.tabBarItem = item;
        [viewControllers addObject:navigationController];
    }
    
    {
        MyCenterViewController *viewController = [[MyCenterViewController alloc] initWithStyle:UITableViewStyleGrouped];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:viewController];
        viewController.title = @"我的";
        
        UIImage *image = [UIImage imageFontAwesomeIconStringForEnum:FAIconUser color:[UIColor cloudsColor] size:30];
        UIImage *selectImage = [UIImage imageFontAwesomeIconStringForEnum:FAIconUser color:[UIColor blueColor] size:30];
        UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:viewController.title image:image selectedImage:selectImage];
        navigationController.tabBarItem = item;
        [viewControllers addObject:navigationController];
    }
    
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    tabBarController.viewControllers = viewControllers;
    for (UINavigationController *viewController in viewControllers) {
        [viewController customUIConfig];
    }
    
    self.window.rootViewController = tabBarController;
}

- (void)checkUserStatus
{
    User *user = [User sharedUser];
    if (user.resignActiveDate == nil || fabs([[NSDate date] timeIntervalSinceDate:user.resignActiveDate]) > 300) { //五分钟后重新登录
        //return;
        if (user.email.length == 0) {
            [self displaySignInView];
        } else {
            [self signIn];
        }
    }
}

- (void)signIn
{
    self.window.userInteractionEnabled = NO;
    self.user = [[User alloc] init];
    __weak __typeof(self)weakSelf = self;
    [self.user signInWithFinishBlock:^(FatherModel *model, NSError *error) {
        
        self.window.userInteractionEnabled = YES;
        
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (!error && [model.code intValue] == 0) {
            [strongSelf dismissSignInView];
        } else {
            [strongSelf displaySignInView];
        }
    }];
}

- (void)displaySignInView
{
    if (self.window.rootViewController.presentedViewController == nil) {
        SignInViewController *signInViewController = [[SignInViewController alloc] init];
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:signInViewController];
        [navigationController customUIConfig];
        [self.window.rootViewController presentViewController:navigationController animated:NO completion:nil];
    }
}

- (void)dismissSignInView
{
    if (self.window.rootViewController.presentedViewController) {
        [self.window.rootViewController.presentedViewController dismissViewControllerAnimated:YES completion:nil];
    }
}

@end