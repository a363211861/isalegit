//
//  Networking.m
//  IOSSun
//
//  Created by Y W on 13-3-20.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "Networking.h"

#import "NetworkingClient.h"

#import "NSData+CommonCrypto.h"
#import "NSData+Base64.h"
#import "Util.h"
#import "b64.h"

#import "User.h"


@interface Networking ()

@property (nonatomic, strong) NSURLSessionDataTask *urlSessionDataTask;

@end


@implementation Networking

- (void)dealloc
{
    [self cancel];
}

//加密
- (NSDictionary *)AES256Encryption:(NSDictionary *)parameters
{
    NSData *data = [NSJSONSerialization dataWithJSONObject:parameters options:NSJSONWritingPrettyPrinted error:nil];
    NSData *aesData = [data AES256EncryptedDataUsingKey:[[NetworkKeyManager shareNetworkKeyManager] AES256Key] error:nil];
    NSString *base64EncodedString = [aesData base64EncodedString];
    
    NSLog(@"%@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSLog(@"\n   %@", base64EncodedString);
    
    if (base64EncodedString == nil) {
        return nil;
    }
    
    return [NSDictionary dictionaryWithObjectsAndKeys:base64EncodedString, @"parameters", nil];
}


//解密
- (NSDictionary *)AES256Decryption:(NSData *)responseData
{
    NSData *aesData = b64_decode(responseData);
    NSData *deData = [aesData decryptedAES256DataUsingKey:[[NetworkKeyManager shareNetworkKeyManager] AES256Key] error:nil];
    
    NSLog(@"%@", [[NSString alloc] initWithData:deData encoding:NSUTF8StringEncoding]);
    
    if (deData == nil) {
        return nil;
    }
    
    return [NSJSONSerialization JSONObjectWithData:deData options:NSJSONReadingAllowFragments error:nil];
}

- (NSString *)signForParameters:(NSDictionary *)parameters
{
    NSArray *keys = [parameters allKeys];
    keys = [keys sortedArrayUsingComparator:^(id obj1, id obj2) {
        return (NSComparisonResult)[(NSString *)obj1 compare:(NSString *)obj2];
    }];
    
    NSMutableString *signString = [[NSMutableString alloc] init];
    [signString appendString:[[NetworkKeyManager shareNetworkKeyManager] AppSecret]];
    for (NSString *str in keys) {
        [signString appendFormat:@"%@%@",str,[parameters objectForKey:str]];
    }
    [signString appendString:[[NetworkKeyManager shareNetworkKeyManager] AppSecret]];
    
    NSString *signStr = [Util md5StringForString:signString];
    return [signStr uppercaseString];
}

//在这个方法里面添加公用到的param
- (NSDictionary *)configParameters:(NSDictionary *)parameters
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [params setObject:@"1.0" forKey:@"v"];
    if ([User sharedUser].token) {
        [params setObject:[User sharedUser].token forKey:@"token"];
    }
    [params setObject:[self signForParameters:params] forKey:@"sign"];
    
    return [self AES256Encryption:params];
}


- (void)cancel
{
    [self.urlSessionDataTask cancel];
    self.urlSessionDataTask = nil;
}

@end




@implementation Networking (GetAndPost)

- (id)initGetParameters:(NSDictionary *)parameters finishBlock:(NetworkFinishBlock)finishBlock
{
    self = [super init];
    if (self) {
        __weak __typeof(self)weakSelf = self;
        self.urlSessionDataTask = [[NetworkingClient sharedClient] GET:urlString parameters:[self configParameters:parameters] success:^(NSURLSessionDataTask *task, id responseObject) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            responseObject = [strongSelf AES256Decryption:responseObject];
            [Util logParams:responseObject];
            if (finishBlock) {
                finishBlock(responseObject, nil);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"CurrentRequest:%@\nError:%@", [task.currentRequest description], [error description]);
            if (finishBlock) {
                finishBlock(nil, error);
            }
        }];
    }
    return self;
}

- (id)initPostParameters:(NSDictionary *)parameters finishBlock:(NetworkFinishBlock)finishBlock
{
    self = [super init];
    if (self) {
        __weak __typeof(self)weakSelf = self;
        self.urlSessionDataTask = [[NetworkingClient sharedClient] POST:urlString parameters:[self configParameters:parameters] success:^(NSURLSessionDataTask *task, id responseObject) {
            __strong __typeof(weakSelf)strongSelf = weakSelf;
            responseObject = [strongSelf AES256Decryption:responseObject];
            [Util logParams:responseObject];
            if (finishBlock) {
                finishBlock(responseObject, nil);
            }
        } failure:^(NSURLSessionDataTask *task, NSError *error) {
            NSLog(@"CurrentRequest:%@\nError:%@", [task.currentRequest description], [error description]);
            if (finishBlock) {
                finishBlock(nil, error);
            }
        }];
    }
    return self;
}

@end



#define kAES256Key @"0eff9a44347901cd91d71cc8df322200"

@implementation NetworkKeyManager

+ (instancetype)shareNetworkKeyManager
{
    static NetworkKeyManager *shareNetworkKeyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareNetworkKeyManager = [[NetworkKeyManager alloc] init];
    });
    return shareNetworkKeyManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.AES256Key = kAES256Key;
        self.AppSecret = [Util md5StringForString:kAES256Key];
    }
    return self;
}

@end
