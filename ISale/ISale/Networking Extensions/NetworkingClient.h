//
//  NetworkingClient.h
//  IOSSun
//
//  Created by Y W on 13-3-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//


#define NetEnvironment 2

#if (NetEnvironment == 1)
//测试环境
static NSString *baseUrl = @"http://gw.api.tbsandbox.com";
static NSString *urlString = @"router/rest";

#elif (NetEnvironment == 2)
//生产环境
static NSString *baseUrl = @"http://gw.api.taobao.com";
static NSString *urlString = @"router/rest";

#else

static NSString *baseUrl = @"http://gw.api.taobao.com";
static NSString *urlString = @"router/rest";

#endif









#import "AFHTTPSessionManager.h"

@interface NetworkingClient : AFHTTPSessionManager

+ (instancetype)sharedClient;

@end