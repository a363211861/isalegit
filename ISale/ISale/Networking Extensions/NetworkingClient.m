//
//  NetworkingClient.m
//  IOSSun
//
//  Created by Y W on 13-3-23.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "NetworkingClient.h"

@implementation NetworkingClient

//单例模式

+ (instancetype)sharedClient;
{
    static NetworkingClient *sharedClient = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedClient = [[NetworkingClient alloc] initWithBaseURL:[NSURL URLWithString:baseUrl]];
        [sharedClient.operationQueue setMaxConcurrentOperationCount:4];
    });
    return sharedClient;
}


//继承

- (id)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    if (self) {
        NSDictionary *headerParamters = [self headerParamters];
        if (headerParamters.count > 0) {
            NSArray *keys = [headerParamters allKeys];
            for (id key in keys) {
                [self.requestSerializer setValue:[headerParamters objectForKey:key] forHTTPHeaderField:key];
            }
        }
    }
    
    return self;
}

- (NSMutableDictionary *)headerParamters
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    return params;
}

@end
