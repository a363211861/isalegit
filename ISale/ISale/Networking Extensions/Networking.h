//
//  Networking.h
//  IOSSun
//
//  Created by Y W on 13-3-20.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

typedef void(^NetworkFinishBlock)(id response, NSError *error);

@interface Networking : NSObject

@end


@interface Networking (GetAndPost)

- (id)initGetParameters:(NSDictionary *)parameters finishBlock:(NetworkFinishBlock)finishBlock;

- (id)initPostParameters:(NSDictionary *)parameters finishBlock:(NetworkFinishBlock)finishBlock;

@end




@interface NetworkKeyManager : NSObject

@property (nonatomic, strong) NSString *AES256Key;
@property (nonatomic, strong) NSString *AppSecret;

+ (instancetype)shareNetworkKeyManager;

@end