//
//  RefreshDateControl.h
//  ISale
//
//  Created by Y W on 13-12-12.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RefreshDateControl : NSObject

+ (RefreshDateControl *)sharedRefreshDateControl;

- (NSDate *)getRefreshDateWithClassDateMD5:(NSString *)classDateMD5;

- (void)setRefreshDate:(NSDate *)refreshDate classDateMD5:(NSString *)classDateMD5;

- (void)deleteRefreshDateWithClassDateMD5:(NSString *)classDateMD5;

@end
