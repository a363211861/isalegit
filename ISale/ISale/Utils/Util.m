//
//  Util.m
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "Util.h"
#import <CommonCrypto/CommonDigest.h>
#import <QuartzCore/QuartzCore.h>

@implementation Util


+ (NSString *)md5StringForString:(NSString *)string
{
    const char *str = [string UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), r);
    return [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
}

+ (void)setExtraCellLineHidden:(UITableView *)tableView
{
    UIView *view =[ [UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    [tableView setTableFooterView:view];
}

+ (UIImage *)viewToImage:(UIView *)view {
    UIGraphicsBeginImageContext(view.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+ (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription
{
    if (localizedDescription == nil || ![localizedDescription isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:localizedDescription, NSLocalizedDescriptionKey, nil];
    return [NSError errorWithDomain:localizedDescription code:-1 userInfo:userInfo];
}

+ (void)logParams:(id)params
{
#ifdef DEBUG
    if ([params isKindOfClass:[NSArray class]] || [params isKindOfClass:[NSDictionary class]]) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSLog(@"%@",jsonString);
    }
#endif
}

@end





@implementation Util (checkValidate)

+ (BOOL)isValidateEmail:(NSString *)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return [emailTest evaluateWithObject:email];
}

+ (BOOL)isValidatePassword:(NSString *)password
{
    NSString *passwordRegex = @"^[a-zA-Z0-9]{6,20}+$";
    NSPredicate *passWordPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passwordRegex];
    return [passWordPredicate evaluateWithObject:password];
}

@end





@implementation Util (date)

+ (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setLocale:[NSLocale currentLocale]];
    [dateFormater setDateFormat:dateFormat];
    return [dateFormater stringFromDate:date];
}

+ (NSDate *)dateFromString:(NSString *)string withDateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    [dateFormater setLocale:[NSLocale currentLocale]];
    [dateFormater setDateFormat:dateFormat];
    return [dateFormater dateFromString:string];
}

@end






@implementation Util (archive)

+ (id)unarchiveObjectWithKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [userDefaults objectForKey:key];
    if (encodedObject == nil) {
        return nil;
    }
    return [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}


+ (BOOL)archiveObject:(id)object withKey:(NSString *)key
{
    NSData *archiveData = [NSKeyedArchiver archivedDataWithRootObject:object];
    if (archiveData == nil) {
        return NO;
    }
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:archiveData forKey:key];
    return [userDefaults synchronize];
}

@end
