//
//  FileUtil.h
//  TelecomCloudManager
//
//  Created by Y W on 13-3-2.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileUtil : NSObject

+ (FileUtil *)sharedFileUtil;

//删除给定的文件夹或者文件
- (BOOL)deleteFilePath:(NSString *)filePath;

//在程序的指定位置创建一个文件夹
- (BOOL)createDirectoryFolder:(NSString *)folderPath;

//在程序的指定文件夹下创建一个文件
- (NSString *)directoryFolder:(NSString *)folderPath fileName:(NSString *)fileName;

//在程序的Documents创建一个文件夹
- (NSString *)createDocumentsDirectoryFolder:(NSString *)folderName;

//在程序的Documents文件区folderName文件夹（没有则创建该文件夹）创建一个文件
- (NSString *)documentsDirectoryFolder:(NSString *)folderName fileName:(NSString *)fileName;

//计算文件的长度
- (unsigned long long)fileSizeForPath:(NSString *)path;

//查看文件是否存在
- (BOOL)fileExistsAtPath:(NSString *)filePath;

//查看目录是否存在
- (BOOL)folderExistsAtPath:(NSString *)folderPath;
@end
