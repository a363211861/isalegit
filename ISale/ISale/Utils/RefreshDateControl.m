//
//  RefreshDateControl.m
//  ISale
//
//  Created by Y W on 13-12-12.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "RefreshDateControl.h"

@interface RefreshDateControl ()

@property (nonatomic, strong)  NSMutableDictionary *classAndDateDictionary;

@end





@implementation RefreshDateControl

+ (RefreshDateControl *)sharedRefreshDateControl
{
    static RefreshDateControl *sharedRefreshDateControl = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedRefreshDateControl = [[RefreshDateControl alloc] init];
    });
    return sharedRefreshDateControl;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.classAndDateDictionary = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSDate *)getRefreshDateWithClassDateMD5:(NSString *)classDateMD5
{
    return [self.classAndDateDictionary objectForKey:classDateMD5];
}

- (void)setRefreshDate:(NSDate *)refreshDate classDateMD5:(NSString *)classDateMD5
{
    [self.classAndDateDictionary setObject:refreshDate forKey:classDateMD5];
}

- (void)deleteRefreshDateWithClassDateMD5:(NSString *)classDateMD5
{
    [self.classAndDateDictionary removeObjectForKey:classDateMD5];
}

@end
