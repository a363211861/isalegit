//
//  FileUtil.m
//  TelecomCloudManager
//
//  Created by Y W on 13-3-2.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FileUtil.h"

@interface FileUtil ()

@property(nonatomic, strong)NSFileManager *fileManager;

@end

@implementation FileUtil

+ (FileUtil *)sharedFileUtil
{
    static FileUtil *sharedFileUtil = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedFileUtil = [[FileUtil alloc] init];
    });
    return sharedFileUtil;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.fileManager = [NSFileManager defaultManager];
    }
    return self;
}

- (BOOL)deleteFilePath:(NSString *)filePath;
{
    if (filePath == nil || ![filePath isKindOfClass:[NSString class]]) {
        return NO;
    }
    
    if (![self.fileManager fileExistsAtPath:filePath isDirectory:nil]) {
        return YES;
    }
    
    return [self.fileManager removeItemAtPath:filePath error:nil];
}


- (BOOL)createDirectoryFolder:(NSString *)folderPath
{
    if (folderPath == nil || ![folderPath isKindOfClass:[NSString class]]) {
        return NO;
    }
    if ([self folderExistsAtPath:folderPath]) {
        return [self.fileManager createDirectoryAtPath:folderPath withIntermediateDirectories:YES attributes:nil error:nil];
    } else {
        return YES;
    }
}


- (NSString *)directoryFolder:(NSString *)folderPath fileName:(NSString *)fileName
{
    if ((folderPath == nil || ![folderPath isKindOfClass:[NSString class]]) || (fileName == nil || ![fileName isKindOfClass:[NSString class]])) {
        return nil;
    }
    
    if ([self createDirectoryFolder:folderPath]) {
        return [folderPath stringByAppendingPathComponent:fileName];
    } else {
        return nil;
    }
}


- (NSString *)createDocumentsDirectoryFolder:(NSString *)folderName
{
    if (folderName == nil || ![folderName isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    if (paths == nil || paths.count == 0) {
        return nil;
    }
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *folderPath = [documentsDirectory stringByAppendingPathComponent:folderName];
    if ([self createDirectoryFolder:folderPath]) {
        return folderPath;
    } else {
        return nil;
    }
}

- (NSString *)documentsDirectoryFolder:(NSString *)folderName fileName:(NSString *)fileName;{
    
    if ((folderName == nil || ![folderName isKindOfClass:[NSString class]]) || (fileName == nil || ![fileName isKindOfClass:[NSString class]])) {
        return nil;
    }
    
    NSString *folderPath = [self createDocumentsDirectoryFolder:folderName];
    if (folderPath == nil || ![folderPath isKindOfClass:[NSString class]]) {
        return nil;
    }
    
    return  [folderPath stringByAppendingPathComponent:fileName];
}

- (unsigned long long)fileSizeForPath:(NSString *)path {
    unsigned long long fileSize = 0;
    if ([self fileExistsAtPath:path]) {
        NSError *error = nil;
        NSDictionary *fileDict = [self.fileManager attributesOfItemAtPath:path error:&error];
        if (!error && fileDict) {
            fileSize = [fileDict fileSize];
        }
    }
    return fileSize;
}

- (BOOL)fileExistsAtPath:(NSString *)filePath
{
    BOOL isDirectory = NO;
    return [self.fileManager fileExistsAtPath:filePath isDirectory:&isDirectory] && !isDirectory;
}

- (BOOL)folderExistsAtPath:(NSString *)folderPath
{
    BOOL isDirectory = NO;
    return [self.fileManager fileExistsAtPath:folderPath isDirectory:&isDirectory] && isDirectory;
}
@end
