//
//  Util.h
//  IOSSun
//
//  Created by Y W on 13-4-9.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Util : NSObject

//生成MD5签名
+ (NSString *)md5StringForString:(NSString *)string;


//去掉UITableview多余的分割线
+ (void)setExtraCellLineHidden:(UITableView *)tableView;


//生成一个本地化描述为LocalizedDescription的NSError
+ (NSError *)errorWithLocalizedDescription:(NSString *)localizedDescription;

//打印json
+ (void)logParams:(id)params;

@end






@interface Util (checkValidate)

//判断是不是邮箱地址
+ (BOOL)isValidateEmail:(NSString *)email;

//判断是不是有效的密码
+ (BOOL)isValidatePassword:(NSString *)password;

@end





@interface Util (date)

+ (NSString *)stringFromDate:(NSDate *)date withDateFormat:(NSString *)dateFormat;
+ (NSDate *)dateFromString:(NSString *)string withDateFormat:(NSString *)dateFormat;

@end






@interface Util (archive)

+ (id)unarchiveObjectWithKey:(NSString *)key;
+ (BOOL)archiveObject:(id)object withKey:(NSString *)key;

@end
