//
//  NetImage.h
//  ISale
//
//  Created by Y W on 13-11-10.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetImage : NSObject

@property (nonatomic, assign) CGSize size;

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSString *imageURLString;

@end