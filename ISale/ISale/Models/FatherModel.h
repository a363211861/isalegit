//
//  FatherModel.h
//  ISale
//
//  Created by Y W on 13-12-12.
//  Copyright (c) 2013年 Y W. All rights reserved.
//


@interface FatherModel : NSObject <NSCoding>

@property (nonatomic, strong) NSString *message; //接口返回的提示信息
@property (nonatomic, strong) NSString *alert; //接口返回的是否将message显示给用户
@property (nonatomic, strong) NSString *code; //接口返回的状态码

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;
- (id)initWithDictionary:(NSDictionary *)dict;
- (NSDictionary *)dictionaryRepresentation;

@end






#import "Util.h"

@interface Util (Model)

+ (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict;

@end








typedef void(^RequestFinishBlock)(FatherModel *model, NSError *error);