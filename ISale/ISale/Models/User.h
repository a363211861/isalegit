//
//  User.h
//  IOSSun
//
//  Created by Y W on 13-7-5.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherModel.h"

@interface User : FatherModel

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *token;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSDate *resignActiveDate;

+ (instancetype)sharedUser;
+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict;

- (void)archiveUser;

@end



@interface User (Functions)

- (void)signInWithFinishBlock:(RequestFinishBlock)finishBlock;
- (void)addInWithFinishBlock:(RequestFinishBlock)finishBlock;

@end