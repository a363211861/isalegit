//
//  Goods.m
//  ISale
//
//  Created by Y W on 13-11-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "Goods.h"
#import "NetImage.h"

@implementation Goods

@synthesize goodsIcon = _goodsIcon;

- (NetImage *)goodsIcon
{
    if (!_goodsIcon) {
        _goodsIcon = [[NetImage alloc] init];
    }
    return _goodsIcon;
}

@end
