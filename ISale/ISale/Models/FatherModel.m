//
//  FatherModel.m
//  ISale
//
//  Created by Y W on 13-12-12.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "FatherModel.h"

NSString *const kFatherModelMessage = @"message";
NSString *const kFatherModelAlert = @"alert";
NSString *const kFatherModelCode = @"code";


@implementation FatherModel

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    FatherModel *instance = [[FatherModel alloc] initWithDictionary:dict];
    return instance;
}

- (id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.message = [Util objectOrNilForKey:kFatherModelMessage fromDictionary:dict];
        self.alert = [Util objectOrNilForKey:kFatherModelAlert fromDictionary:dict];
        self.code = [Util objectOrNilForKey:kFatherModelCode fromDictionary:dict];
    }
    
    return self;
}


- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];
    [mutableDict setValue:self.message forKey:kFatherModelMessage];
    [mutableDict setValue:self.alert forKey:kFatherModelAlert];
    [mutableDict setValue:self.code forKey:kFatherModelCode];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"%@", [self dictionaryRepresentation]];
}


#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.message = [aDecoder decodeObjectForKey:kFatherModelMessage];
    self.alert = [aDecoder decodeObjectForKey:kFatherModelAlert];
    self.code = [aDecoder decodeObjectForKey:kFatherModelCode];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.message forKey:kFatherModelMessage];
    [aCoder encodeObject:self.alert forKey:kFatherModelAlert];
    [aCoder encodeObject:self.code forKey:kFatherModelCode];
}

@end


@implementation Util (Model)

+ (id)objectOrNilForKey:(id)aKey fromDictionary:(NSDictionary *)dict
{
    id object = [dict objectForKey:aKey];
    return [object isEqual:[NSNull null]] ? nil : object;
}

@end