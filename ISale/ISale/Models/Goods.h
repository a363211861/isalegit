//
//  Goods.h
//  ISale
//
//  Created by Y W on 13-11-6.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kGoodsListCellIconSize 48
#define kGoodsCollectionViewCellIconSize 145

@class NetImage;

@interface Goods : NSObject

@property (nonatomic, strong) NSString *goodsName;
@property (nonatomic, strong, readonly) NetImage *goodsIcon;
@property (nonatomic, strong) NSString *artist;

@property (nonatomic, strong) NSArray *images;

@end
