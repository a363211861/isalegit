//
//  User.m
//  IOSSun
//
//  Created by Y W on 13-7-5.
//  Copyright (c) 2013年 Y W. All rights reserved.
//

#import "User.h"
#import "Util.h"
#import "Networking.h"
#import "NSData+CommonCrypto.h"
#import "NSData+Base64.h"
#import "b64.h"



NSString *const kUserEmail = @"email";
NSString *const kUserPassword = @"password";
NSString *const kUserToken = @"token";
NSString *const kKey = @"key";
NSString *const kUserResignActiveDate = @"resignActiveDate";
NSString *const kUserDefaultUser = @"defaultUser";



@implementation User

+ (instancetype)sharedUser
{
    static User *sharedUser = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedUser = [Util unarchiveObjectWithKey:kUserDefaultUser];
        if (sharedUser == nil) {
            sharedUser = [[User alloc] init];
        }
    });
    return sharedUser;
}

+ (instancetype)modelObjectWithDictionary:(NSDictionary *)dict
{
    User *instance = [[User alloc] initWithDictionary:dict];
    return instance;
}


- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self && [dict isKindOfClass:[NSDictionary class]]) {
        self.token = [Util objectOrNilForKey:kUserToken fromDictionary:dict];
        self.key = [Util objectOrNilForKey:kKey fromDictionary:dict];
    }
    
    return self;
}

- (NSDictionary *)dictionaryRepresentation
{
    NSDictionary *superDictionaryRepresentation = [super dictionaryRepresentation];
    
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionaryWithDictionary:superDictionaryRepresentation];
    [mutableDict setValue:self.email forKey:kUserEmail];
    [mutableDict setValue:self.password forKey:kUserPassword];
    [mutableDict setValue:self.token forKey:kUserToken];
    [mutableDict setValue:self.key forKey:kKey];
    [mutableDict setValue:self.resignActiveDate forKey:kUserResignActiveDate];
    
    return [NSDictionary dictionaryWithDictionary:mutableDict];
}

- (void)archiveUser
{
    [Util archiveObject:self withKey:kUserDefaultUser];
}

#pragma mark - NSCoding Methods

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    self.email = [aDecoder decodeObjectForKey:kUserEmail];
    self.password = [aDecoder decodeObjectForKey:kUserPassword];
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.email forKey:kUserEmail];
    [aCoder encodeObject:self.password forKey:kUserPassword];
}

@end






#import "NSObject+Category.h"

NSString *const kNetworkingKey = @"networking";

@implementation User (Functions)

- (void)clearNetworking
{
    [self setValue:nil forKey:kNetworkingKey];
}

- (void)configNetworkKey
{
    [NetworkKeyManager shareNetworkKeyManager].AES256Key = self.key;
    [NetworkKeyManager shareNetworkKeyManager].AppSecret = [Util md5StringForString:[NSString stringWithFormat:@"%@%@%@", self.key, self.email, self.password]];
}

- (void)signInWithFinishBlock:(RequestFinishBlock)finishBlock
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"login" forKey:@"method"];
    [parameters setObject:self.email forKey:@"email"];
    [parameters setObject:self.password forKey:@"password"];
    
    __weak __typeof(self)weakSelf = self;
    Networking *networking = [[Networking alloc] initGetParameters:parameters finishBlock:^(id response, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (error) {
            if (finishBlock) {
                finishBlock(nil, error);
            }
            [strongSelf clearNetworking];
            return;
        }
        
        User *resultModel = [[User alloc] initWithDictionary:response];
        strongSelf.message = resultModel.message;
        strongSelf.alert = resultModel.alert;
        strongSelf.code = resultModel.code;
        strongSelf.token = resultModel.token;
        strongSelf.key = resultModel.key;
        
        if ([strongSelf.code intValue] == 0) {
            [strongSelf archiveUser];
            [self configNetworkKey];
        }
        
        if (finishBlock) {
            finishBlock(strongSelf, nil);
        }
        [strongSelf clearNetworking];
    }];
    [self setValue:networking forKey:kNetworkingKey];
}

- (void)addInWithFinishBlock:(RequestFinishBlock)finishBlock
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setObject:@"register" forKey:@"method"];
    [parameters setObject:self.email forKey:@"email"];
    [parameters setObject:self.password forKey:@"password"];
    
    __weak __typeof(self)weakSelf = self;
    Networking *networking = [[Networking alloc] initGetParameters:parameters finishBlock:^(id response, NSError *error) {
        __strong __typeof(weakSelf)strongSelf = weakSelf;
        if (error) {
            if (finishBlock) {
                finishBlock(nil, error);
            }
            [strongSelf clearNetworking];
            return;
        }
        
        User *resultModel = [[User alloc] initWithDictionary:response];
        strongSelf.message = resultModel.message;
        strongSelf.alert = resultModel.alert;
        strongSelf.code = resultModel.code;
        strongSelf.token = resultModel.token;
        
        if ([strongSelf.code intValue] == 0) {
            [strongSelf archiveUser];
            [self configNetworkKey];
        }
        
        if (finishBlock) {
            finishBlock(strongSelf, nil);
        }
        [strongSelf clearNetworking];
    }];
    [self setValue:networking forKey:kNetworkingKey];
}

@end