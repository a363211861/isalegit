//
//  Waiting.m
//  Nav
//
//  Created by frank on 11-6-30.
//  Copyright 2011 wongf70@gmail.com All rights reserved.
//

#import "WWaitingBar.h"
#import <math.h>

@implementation WWaitingBar

- (void)shift
{
	timeCount = (timeCount + 1) % ballNum;
	[[self viewWithTag:timeCount + 10].layer addAnimation:[animationArray objectAtIndex:0] forKey:@"scale"];
	[[self viewWithTag:timeCount + 10].layer addAnimation:[animationArray objectAtIndex:1] forKey:@"alpha"];
}

- (id)initWithRadius:(float)aRadius color:(UIColor *)aColor timeInterval:(float)aInterval
{
    self = [super initWithFrame:CGRectMake(-aRadius, -aRadius, 2.0 * aRadius, 2.0 * aRadius)];
    if (!self) {
        return nil;
    }
    self.userInteractionEnabled = NO;
	self.backgroundColor = [UIColor clearColor];
	
	ballNum = 12;
	CABasicAnimation *scaleAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
	scaleAnimation.duration = 1.5 * aInterval;
	scaleAnimation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeScale(1.4, 1.4, 1.0)];
	scaleAnimation.autoreverses = YES;
	scaleAnimation.removedOnCompletion = YES;
	CABasicAnimation *alphaAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
	alphaAnimation.duration = 10.0 * aInterval / ballNum;
	alphaAnimation.toValue=[NSNumber numberWithFloat:1.0];
	alphaAnimation.autoreverses = YES;
	alphaAnimation.removedOnCompletion = YES;
	animationArray = [[NSArray alloc] initWithObjects:scaleAnimation, alphaAnimation, nil];

	for (int i = 0; i < ballNum; i++)
	{
		UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(-aRadius / 3.5, -aRadius / 16.0, aRadius / 1.75, aRadius / 8.0)];
		tempView.layer.borderWidth = 0;
		tempView.layer.cornerRadius = aRadius/18.0;
		tempView.layer.anchorPoint = CGPointMake(0.5, 0.5);
		tempView.tag = i + 10;
		tempView.backgroundColor = aColor;
		tempView.alpha = 0.4;
		float angle = i * M_PI * 2 / ballNum;
		CATransform3D translation3D = CATransform3DMakeTranslation(0.7 * aRadius * cos(angle), 0.7 * aRadius * sin(angle), 0);
		CATransform3D rotation3D = CATransform3DRotate(translation3D, angle, 0, 0, 1);
		tempView.layer.transform = rotation3D;
		[self addSubview:tempView];
	}
	[NSTimer scheduledTimerWithTimeInterval:aInterval target:self selector:@selector(shift) userInfo:nil repeats:YES];
    return self;
}

@end
