//
//  UIImage+FontAwesome.m
//  ISale
//
//  Created by Y W on 14-2-17.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import "UIImage+FontAwesome.h"

#import "UIImage+Category.h"

#import "UIFont+FontAwesome.h"

@implementation UIImage (FontAwesome)

+ (instancetype)imageFontAwesomeIconStringForEnum:(FAIcon)value color:(UIColor *)color size:(float)size
{
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = color;
    label.font = [UIFont fontAwesomeFontOfSize:size * 2];
    label.text = [NSString fontAwesomeIconStringForEnum:value];
    CGSize labelSize = [label.text sizeWithAttributes:[NSDictionary dictionaryWithObject:label.font forKey:NSFontAttributeName]];
    CGRect rect = CGRectZero;
    rect.size.width = ceilf(labelSize.width);
    rect.size.height = ceilf(labelSize.height);
    label.frame = rect;
    
    UIImage *image = [UIImage imageWithView:label];
    
    return [[UIImage alloc] initWithCGImage:image.CGImage scale:2 orientation:image.imageOrientation];
}

@end
