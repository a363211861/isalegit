//
//  UIImage+FontAwesome.h
//  ISale
//
//  Created by Y W on 14-2-17.
//  Copyright (c) 2014年 Y W. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NSString+FontAwesome.h"

@interface UIImage (FontAwesome)

+ (instancetype)imageFontAwesomeIconStringForEnum:(FAIcon)value color:(UIColor *)color size:(float)size;

@end
